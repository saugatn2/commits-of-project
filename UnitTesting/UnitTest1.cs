﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CmdUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MultiparseTest()
        {

            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "drawto 10,10", myCanvas);
        }

        [TestMethod]
        public void ParameterSeperator()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "drawto 10,10", myCanvas);

        }

        [TestMethod]
        public void MoveToTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("moveto 10,10", "", myCanvas);

        }

        [TestMethod]
        public void RectangleTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("rectangle 10,10", "", myCanvas);
            cmdSetup.parse("square 10", "", myCanvas);
        }

        [TestMethod]
        public void CircleTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("circle 10", "", myCanvas);
        }

        [TestMethod]
        public void TriangleTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("drawto 100,80,20", "", myCanvas);
        }

        [TestMethod]
        public void ColorTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("pen black", "", myCanvas);
        }

        [TestMethod]
        public void FillTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("fill on", "", myCanvas);
        }

        [TestMethod]
        public void ClearTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("clear", "", myCanvas);
        }

        [TestMethod]
        public void ResetTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("reset", "", myCanvas);
        }

        [TestMethod]
        public void RunTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();

            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas myCanvas = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "", myCanvas);
        }

        [TestMethod]
        public void VarStoreTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();
            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;

            Application_graphical.Canvas MyCanvass = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("value = 10", "", MyCanvass);
        }

        [TestMethod]
        public void VarAppendTest()
        {
            Application_graphical.Form1 form = new Application_graphical.Form1();
            Application_graphical.Parser cmdSetup = new Application_graphical.Parser();
            Bitmap outBitmap = form.myBitmap;
            Application_graphical.Canvas MyCanvass = new Application_graphical.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("value = value + 10", "", MyCanvass);
            cmdSetup.parse("value = value - 10", "", MyCanvass);
            cmdSetup.parse("value = value / 10", "", MyCanvass);
            cmdSetup.parse("value = value * 10", "", MyCanvass);
        }
    }
}