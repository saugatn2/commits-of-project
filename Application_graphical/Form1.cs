﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_graphical
{
    /// <summary>
    /// Created Form1 class which is inherits from Form 
    /// This class hold method and properties for bitmap, slider, Menu bar and Enter event on command
    /// Prompt
    /// </summary>
    public partial class Form1 : Form
    {
        //Instance data for slide show
        int pnl_SlideWidth;
        bool Hided;

        //Bitmap to draw which will display on pictureBox
        const int bitmapX = 640;
        const int bitmapY = 480;
        public Bitmap myBitmap = new Bitmap(bitmapX, bitmapY);
        public Graphics g;

        Canvas myCanvas;


        public Form1()
        {
            InitializeComponent();
            pnl_SlideWidth = pnl_Slide.Width;
            Hided = false;
            myCanvas = new Canvas(Graphics.FromImage(myBitmap));
        }


        

        private void commandPrompt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ObtainInstruction(); //call ObtainInstruction Method
            }
        }


        public void ObtainInstruction()
        {
            String readCommand = commandPrompt.Text.Trim().ToLower(); //fetches input of single line text field 
            String readMultiCommand = programWindow.Text.Trim().ToLower(); //fetches input of multi line text field 
            Parser cmdSetup = new Parser();  //creating obj of Parser
            cmdSetup.parse(readCommand, readMultiCommand, myCanvas); //calling a method of Parser and passing both textfield's input and alsoonject of canva
            Refresh();
        }


        private void outputWindow_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            g.DrawImageUnscaled(myBitmap, 0, 0);
        }


        private void Btn_Run_Click(object sender, EventArgs e)
        {
            ObtainInstruction();
        }


        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                outputWindow.Load(openFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.ShowDialog();
                myBitmap.Save(saveFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit(); //Application get exit 
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }


        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Application: Graphical Application\n"

                + Environment.NewLine + "Developer: Saugat Neupane"
                , "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void Exit_btn_Click(object sender, EventArgs e)
        {
            Application.Exit();  // Terminate Application 
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void programWindow_TextChanged(object sender, EventArgs e)
        {

        }

        private void outputWindow_Click(object sender, EventArgs e)
        {

        }

        private void commandPrompt_TextChanged(object sender, EventArgs e)
        {

        }
    }
}